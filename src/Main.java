import java.util.ArrayList;
import java.util.Scanner;

public class Main extends Shape{
	
	static ArrayList<Shape> shp = new ArrayList<Shape>();
	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		Shape shap = new Shape();
		
		while (choice != 3) {
			// 1. show the menu
			showMenu();
	
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();
			
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			System.out.println();
			
			if(choice == 1)
			{
			System.out.println("Enter value for base of triangle");
			int base = keyboard.nextInt();
			
			System.out.println("Enter value for height of triangle");
			int height = keyboard.nextInt();
			
			Triangle tri = new Triangle(base,height);
			
			//ArrayList<Shape> shp = new ArrayList<Shape>();
			shp.add(tri);
			 
		
			}
			else if(choice == 2)
			{
				System.out.println("Enter value for side of square");
				int side = keyboard.nextInt();
				
				Square sqr = new Square(side);

				shp.add(sqr);
			}	
			
			shap.setShapes(shp);
			
			ArrayList<Shape> shapes = shap.getShapes();
			
			 for(Shape sh: shapes)
				{				
				 sh.printInfo();			 				 
				}

		}
	}
	
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
//		System.out.println("3. Rectangle");
		System.out.println("3. Exit");
	}

}
