import java.util.ArrayList;

public class Shape {
	private String color;
	private int dimensions;
	private ArrayList shapes;
	
	public ArrayList getShapes() {
		return shapes;
	}

	public void setShapes(ArrayList shapes) {
		this.shapes = shapes;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public int getDimensions() {
		return dimensions;
	}

	public void setDimensions(int dimensions) {
		dimensions = dimensions;
	}
	
	public void printInfo()
	{
		
	}
//	public void printInfo()
//	{
//		//TwoDimensionalShapeInterface intrfc = new Square();
//		Shape shp = new Shape();
//		shp.setColor("red");
//		shp.setDimensions(2);
//		
//		System.out.println("The area of the square is:"+calculateArea());
//		System.out.println("The color of the square is:"+shp.getColor());
//		System.out.println("The dimensions of the square is:"+shp.getDimensions());
//	
//	}
}
