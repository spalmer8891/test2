
public class Square extends Shape implements TwoDimensionalShapeInterface{

	private int side;
	
	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	Square()
	{
		
	}
	
	Square(int side)
	{
		this.side = side;
	}
	
	public double calculateArea() {
		
		int area = this.side * this.side;
	 return area;
	}
	
	public void printInfo()
	{
		super.printInfo();
		Shape shp = new Shape();
		shp.setColor("red");
		shp.setDimensions(2);
		System.out.println("The area of the square is:"+calculateArea());
		System.out.println("The color of the square is:"+shp.getColor());
		System.out.println("The dimensions of the square is:"+shp.getDimensions());
	
	}
}
