public class Triangle extends Shape implements TwoDimensionalShapeInterface {

	private int base;
	private int height;
	
	
	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	Triangle()
	{
		
	}
	
	Triangle(int base, int height)
	{
		this.base = base; 
		this.height =height;
	}
	
	

	public double calculateArea() {
		
		 double area = (this.base / 2) * this.height;
		 return area;
		}
		
		public void printInfo()
		{
			super.printInfo();
			Shape shp = new Shape();
			shp.setColor("yellow");
			shp.setDimensions(2);
			System.out.println("The area of the triangle is:"+calculateArea());
			System.out.println("The color of the triangle is:"+shp.getColor());
			System.out.println("The dimensions of the triangle is:"+shp.getDimensions());
		
		}
}
